<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210402104824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE subscription_subscriptions ADD web_hook_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subscription_subscriptions ADD CONSTRAINT FK_6A72DAFA5A7D4251 FOREIGN KEY (web_hook_id) REFERENCES subscription_web_hooks (id)');
        $this->addSql('CREATE INDEX IDX_6A72DAFA5A7D4251 ON subscription_subscriptions (web_hook_id)');
        $this->addSql('ALTER TABLE subscription_web_hooks CHANGE data data LONGTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE subscription_subscriptions DROP FOREIGN KEY FK_6A72DAFA5A7D4251');
        $this->addSql('DROP INDEX IDX_6A72DAFA5A7D4251 ON subscription_subscriptions');
        $this->addSql('ALTER TABLE subscription_subscriptions DROP web_hook_id');
        $this->addSql('ALTER TABLE subscription_web_hooks CHANGE data data VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}

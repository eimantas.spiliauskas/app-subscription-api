<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103181624 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE subscription_subscriptions (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, product_id VARCHAR(255) NOT NULL, provider_identifier VARCHAR(255) NOT NULL, provider VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, create_at VARCHAR(255) NOT NULL, updated_at VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_6A72DAFA5F37A13B (token), UNIQUE INDEX UNIQ_6A72DAFA5298CFD2 (provider_identifier), INDEX IDX_6A72DAFAA76ED395 (user_id), UNIQUE INDEX UNIQ_6A72DAFAA76ED3954584665A92C4739C (user_id, product_id, provider), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscription_users (id INT AUTO_INCREMENT NOT NULL, token VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_EEBBA49A5F37A13B (token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subscription_subscriptions ADD CONSTRAINT FK_6A72DAFAA76ED395 FOREIGN KEY (user_id) REFERENCES subscription_users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE subscription_subscriptions DROP FOREIGN KEY FK_6A72DAFAA76ED395');
        $this->addSql('DROP TABLE subscription_subscriptions');
        $this->addSql('DROP TABLE subscription_users');
    }
}

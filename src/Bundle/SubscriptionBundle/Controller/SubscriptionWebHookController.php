<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Controller;

use App\Bundle\SubscriptionBundle\Normalizer\WebHookFilterNormalizer;
use App\Bundle\SubscriptionBundle\Repository\WebHookRepository;
use App\Bundle\SubscriptionBundle\Service\SubscriptionNotificationHandler;
use App\Bundle\SubscriptionBundle\Service\WebHookManager;
use App\Normalizer\FilterResultNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionWebHookController
{
    private SubscriptionNotificationHandler $notificationHandler;
    private EntityManagerInterface $entityManager;
    private WebHookFilterNormalizer $webHookFilterNormalizer;
    private WebHookManager $webHookManager;
    private FilterResultNormalizer $filterResultNormalizer;
    private WebHookRepository $webHookRepository;

    public function __construct(
        SubscriptionNotificationHandler $notificationHandler,
        EntityManagerInterface $entityManager,
        WebHookFilterNormalizer $webHookFilterNormalizer,
        WebHookManager $webHookManager,
        FilterResultNormalizer $filterResultNormalizer,
        WebHookRepository $webHookRepository
    ) {
        $this->notificationHandler = $notificationHandler;
        $this->entityManager = $entityManager;
        $this->webHookFilterNormalizer = $webHookFilterNormalizer;
        $this->webHookManager = $webHookManager;
        $this->filterResultNormalizer = $filterResultNormalizer;
        $this->webHookRepository = $webHookRepository;
    }

    public function createWebHook(Request $request, string $provider): Response
    {
        $this->webHookManager->createWebHookFromRequest($request, $provider);

        $this->entityManager->flush();

        return $this->notificationHandler->getResponse($provider);
    }

    public function getWebHooks(Request $request)
    {
        $query = $request->query->all();
        $webHookFilter = $this->webHookFilterNormalizer->mapToEntity($query);

        $result = $this->webHookManager->findByFilter($webHookFilter);
        $resultArray = $this->filterResultNormalizer->mapFromEntity($result);

        return (new JsonResponse())->setData($resultArray);
    }

    public function processWebHook(string $hash)
    {
        $webHook = $this->webHookRepository->findNewByToken($hash);

        if ($webHook === null) {
            return new JsonResponse('', Response::HTTP_NOT_FOUND);
        }

        $this->webHookManager->processWebHook($webHook);

        $this->entityManager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}

<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Repository;

use App\Bundle\SubscriptionBundle\Entity\Subscription;
use Doctrine\ORM\EntityRepository;

class SubscriptionRepository extends EntityRepository
{
    public function findByIdentifier(string $identifier): ?Subscription
    {
        return $this->createQueryBuilder()
            ->where('')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
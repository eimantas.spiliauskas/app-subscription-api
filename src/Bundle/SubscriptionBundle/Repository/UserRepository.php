<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getById(int $id)
    {
        return $this->find($id);
    }
}
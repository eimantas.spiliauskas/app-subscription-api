<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Repository;

use App\Bundle\SubscriptionBundle\Entity\WebHook;
use App\Bundle\SubscriptionBundle\Entity\WebHookFilter;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class WebHookRepository extends EntityRepository
{
    public function findNewByToken(string $hash): ?WebHook
    {
        return $this->createQueryBuilder('wh')
            ->where('wh.hash = :hash')
            ->andWhere('wh.status = :status')
            ->setParameter('hash', $hash)
            ->setParameter('status', WebHook::STATUS_NEW)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findCountByFilter(WebHookFilter $webHookFilter): int
    {
        return (int) $this->getQueryBuilderByFilter($webHookFilter)
            ->select('COUNT(wh)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @param WebHookFilter $webHookFilter
     * @return WebHook[]
     */
    public function findByFilter(WebHookFilter $webHookFilter): array
    {
        return $this->getQueryBuilderByFilter($webHookFilter)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getQueryBuilderByFilter(WebHookFilter $webHookFilter): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('wh');

        if ($webHookFilter->getProvider() !== null) {
            $queryBuilder
                ->andWhere('wh.provider = :provider')
                ->setParameter('provider', $webHookFilter->getProvider())
            ;
        }

        if ($webHookFilter->getLimit() !== null) {
            $queryBuilder
                ->setMaxResults($webHookFilter->getLimit())
            ;
        }

        if ($webHookFilter->getStatus() !== null) {
            $queryBuilder
                ->andWhere('wh.status = :status')
                ->setParameter('status', $webHookFilter->getStatus())
            ;
        }

        return $queryBuilder;
    }
}

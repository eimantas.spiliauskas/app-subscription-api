<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Normalizer;

use App\Bundle\SubscriptionBundle\Entity\WebHook;
use App\Normalizer\DenormalizerInterface;

class WebHookNormalizer implements DenormalizerInterface
{
    /**
     * @param WebHook $data
     * @return array
     */
    public function mapFromEntity(object $data): array
    {
        return [
            'data' => $data->getData(),
            'status' => $data->getStatus(),
            'hash' => $data->getHash(),
            'provider' => $data->getProvider(),
            'created_at' => $data->getCreatedAt()->getTimestamp(),
        ];
    }
}
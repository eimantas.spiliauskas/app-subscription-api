<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Normalizer;

use App\Bundle\SubscriptionBundle\Entity\WebHookFilter;
use App\Normalizer\FilterNormalizer;
use App\Normalizer\NormalizerInterface;

class WebHookFilterNormalizer implements NormalizerInterface
{
    private FilterNormalizer $filterNormalizer;

    public function __construct(FilterNormalizer $filterNormalizer)
    {
        $this->filterNormalizer = $filterNormalizer;
    }

    /**
     * @param array $data
     * @return WebHookFilter
     */
    public function mapToEntity(array $data): object
    {
        $webHookFilter = new WebHookFilter();

        $this->filterNormalizer->mapBase($webHookFilter, $data);

        if (!empty($data['provider'])) {
            $webHookFilter->setProvider($data['provider']);
        }

        if (!empty($data['status'])) {
            $webHookFilter->setStatus($data['status']);
        }

        return $webHookFilter;
    }
}
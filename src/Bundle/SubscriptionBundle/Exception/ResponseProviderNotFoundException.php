<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Exception;

use Exception;

class ResponseProviderNotFoundException extends Exception
{

}
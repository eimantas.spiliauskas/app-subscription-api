<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle;

use App\Bundle\SubscriptionBundle\Entity\Notification;
use InvalidArgumentException;

final class StatusResolver
{
    const APPLE_STATUS_CANCEL = 'CANCEL';
    const APPLE_STATUS_DID_RENEW = 'DID_RENEW';
    const APPLE_STATUS_INITIAL_BUY = 'INITIAL_BUY';
    const DID_FAIL_TO_RENEW = 'DID_FAIL_TO_RENEW';

    private static array $statusNotificationStatusMap = [
        self::APPLE_STATUS_CANCEL => Notification::TYPE_CANCEL,
        self::APPLE_STATUS_DID_RENEW => Notification::TYPE_UPDATE,
        self::APPLE_STATUS_INITIAL_BUY => Notification::TYPE_CREATE,
        self::DID_FAIL_TO_RENEW => Notification::TYPE_CANCEL,
    ];

    public static function getNotificationStatus(string $status): string
    {
        if (!isset(self::$statusNotificationStatusMap[$status])) {
            throw new InvalidArgumentException('Status not found');
        }

        return self::$statusNotificationStatusMap[$status];
    }
}
<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Entity;

use DateTime;

class Notification
{
    const TYPE_CREATE = 'create';
    const TYPE_UPDATE = 'update';
    const TYPE_CANCEL = 'cancel';

    private string $product;
    private string $subscriptionIdentifier;
    private string $type;
    private string $provider;
    private DateTime $date;

    public function getProduct(): string
    {
        return $this->product;
    }

    public function setProduct(string $product): Notification
    {
        $this->product = $product;
        return $this;
    }


    public function getSubscriptionIdentifier(): string
    {
        return $this->subscriptionIdentifier;
    }

    public function setSubscriptionIdentifier(string $subscriptionIdentifier): Notification
    {
        $this->subscriptionIdentifier = $subscriptionIdentifier;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): Notification
    {
        $this->type = $type;
        return $this;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function setProvider(string $provider): Notification
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Notification
     */
    public function setDate(DateTime $date): Notification
    {
        $this->date = $date;
        return $this;
    }
}

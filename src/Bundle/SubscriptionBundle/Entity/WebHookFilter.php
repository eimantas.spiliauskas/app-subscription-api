<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Entity;

use App\Entity\Filter;

class WebHookFilter extends Filter
{
    private ?string $provider;
    private ?string $status;

    public function __construct()
    {
        parent::__construct();

        $this->provider = null;
        $this->status = null;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function setProvider(?string $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Entity;

use DateTime;

class Subscription
{
    const STATUS_ACTIVE = 'active';
    const STATUS_CANCELED = 'canceled';

    private int $id;
    private string $token;
    private User $user;
    private string $productId;
    private string $providerIdentifier;
    private string $provider;
    private string $status;
    private DateTime $createdAt;
    private ?DateTime $updatedAt;
    private WebHook $webHook;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->updatedAt = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $product): self
    {
        $this->productId = $product;
        return $this;
    }

    public function getProviderIdentifier(): string
    {
        return $this->providerIdentifier;
    }

    public function setProviderIdentifier(string $providerIdentifier): Subscription
    {
        $this->providerIdentifier = $providerIdentifier;
        return $this;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function setProvider(string $provider): Subscription
    {
        $this->provider = $provider;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getWebHook(): WebHook
    {
        return $this->webHook;
    }

    public function setWebHook(WebHook $webHook): self
    {
        $this->webHook = $webHook;
        return $this;
    }
}

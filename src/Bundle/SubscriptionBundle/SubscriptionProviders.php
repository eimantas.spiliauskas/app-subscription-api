<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle;

final class SubscriptionProviders
{
    const PROVIDER_APPLE = 'apple';

    public static function getAvailableProviders(): array
    {
        return [
            self::PROVIDER_APPLE,
        ];
    }
}
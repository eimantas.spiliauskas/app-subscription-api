<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service;

use App\Bundle\SubscriptionBundle\Entity\Subscription;
use App\Bundle\SubscriptionBundle\Entity\WebHook;
use App\Bundle\SubscriptionBundle\Exception\UnconfiguredProviderException;
use App\Bundle\SubscriptionBundle\Service\Decoder\Decoder;
use App\Bundle\SubscriptionBundle\Service\Mapper\Mapper;
use App\Bundle\SubscriptionBundle\Service\Response\ResponseProvider;
use App\Bundle\SubscriptionBundle\Service\Validator\SubscriptionNotificationRequestValidator;
use App\Bundle\SubscriptionBundle\SubscriptionProviders;
use Symfony\Component\HttpFoundation\Request;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionNotificationHandler
{
    private SubscriptionNotificationRequestValidator $requestValidator;
    private Mapper $mapper;
    private SubscriptionLifeCircleManager $subscriptionLifeCircleManager;
    private ResponseProvider $responseProvider;

    public function __construct(
        SubscriptionNotificationRequestValidator $requestValidator,
        Mapper $mapper,
        SubscriptionLifeCircleManager $subscriptionLifeCircleManager,
        ResponseProvider $responseProvider
    ) {
        $this->requestValidator = $requestValidator;
        $this->mapper = $mapper;
        $this->subscriptionLifeCircleManager = $subscriptionLifeCircleManager;
        $this->responseProvider = $responseProvider;
    }

    public function validate(Request $request, array $decodedBody, $provider)
    {
        if (!in_array($provider, SubscriptionProviders::getAvailableProviders())) {
            throw new UnconfiguredProviderException('provider not configured: ' . $provider);
        }

        if (!$this->requestValidator->isValid($request, $decodedBody, $provider)) {
            throw new InvalidArgumentException('Invalid notification request. Provider: ' . $provider);
        }
    }

    public function getResponse(string $provider): Response
    {
        return $this->responseProvider->getResponse($provider);
    }

    public function handleFromWebHook(WebHook $webHook): Subscription
    {
        $notification = $this->mapper->map($webHook->getData(), $webHook->getProvider());
        return $this->subscriptionLifeCircleManager->handleNotification($notification);
    }
}

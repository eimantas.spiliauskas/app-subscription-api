<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Response;

use App\Bundle\SubscriptionBundle\Exception\ResponseProviderNotFoundException;
use Symfony\Component\HttpFoundation\Response;

class ResponseProvider
{
    private array $responses;

    public function __construct()
    {
        $this->responses = [];
    }

    public function addResponse(ResponseProviderInterface $response, string $provider): void
    {
        $this->responses[$provider] = $response;
    }

    public function getResponse(string $provider): Response
    {
        $responseProvider = $this->getResponseProvider($provider);
        return $responseProvider->getSuccessResponse($provider);
    }

    public function getResponseProvider(string $provider): ResponseProviderInterface
    {
        if (!isset($this->responses[$provider])) {
            throw new ResponseProviderNotFoundException('Response provider not found by type: ' . $provider);
        }

        return $this->responses[$provider];
    }
}
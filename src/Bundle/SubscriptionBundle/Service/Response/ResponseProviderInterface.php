<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Response;

use Symfony\Component\HttpFoundation\Response;

interface ResponseProviderInterface
{
    public function getSuccessResponse(string $provider): Response;
}
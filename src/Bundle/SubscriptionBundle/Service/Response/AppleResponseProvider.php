<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Response;

use Symfony\Component\HttpFoundation\Response;

class AppleResponseProvider implements ResponseProviderInterface
{
    public function getSuccessResponse(string $provider): Response
    {
        return new Response();
    }
}
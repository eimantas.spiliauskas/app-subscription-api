<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service;

use App\Bundle\SubscriptionBundle\Entity\WebHook;
use App\Bundle\SubscriptionBundle\Entity\WebHookFilter;
use App\Bundle\SubscriptionBundle\Repository\WebHookRepository;
use App\Bundle\SubscriptionBundle\Service\Decoder\Decoder;
use App\Entity\FilterResult;
use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

class WebHookManager
{
    private WebHookRepository $webHookRepository;
    private SubscriptionNotificationHandler $subscriptionNotificationHandler;
    private Decoder $decoderManager;
    private EntityManager $entityManager;

    public function __construct(
        WebHookRepository $webHookRepository,
        SubscriptionNotificationHandler $subscriptionNotificationHandler,
        Decoder $decoderManager,
        EntityManager $entityManager
    ) {
        $this->webHookRepository = $webHookRepository;
        $this->subscriptionNotificationHandler = $subscriptionNotificationHandler;
        $this->decoderManager = $decoderManager;
        $this->entityManager = $entityManager;
    }

    public function findByFilter(WebHookFilter $filter)
    {
        $result = $this->webHookRepository->findByFilter($filter);
        $totalCount = $this->webHookRepository->findCountByFilter($filter);

        return (new FilterResult())
            ->setItems($result)
            ->setTotalCount($totalCount)
        ;
    }

    public function processWebHook(WebHook $webHook)
    {
        $subscription = $this->subscriptionNotificationHandler->handleFromWebHook($webHook);
        $webHook->setStatus(WebHook::STATUS_PROCESSED);
        $subscription->setWebHook($webHook);
    }

    public function createWebHookFromRequest(Request $request, string $provider): WebHook
    {
        $body = $request->getContent();
        $decodedBody = $this->decoderManager->decode($body, $provider);

        $this->subscriptionNotificationHandler->validate($request, $decodedBody, $provider);

        $webHook = (new WebHook())
            ->setStatus(WebHook::STATUS_NEW)
            ->setData($decodedBody)
            ->setHash(Uuid::uuid4()->toString())
            ->setProvider($provider)
        ;

        $this->entityManager->persist($webHook);

        return $webHook;
    }
}

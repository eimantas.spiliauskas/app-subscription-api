<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Validator;

use Symfony\Component\HttpFoundation\Request;

class AppleNotificationValidator implements NotificationValidatorInterface
{
    public function isValid(Request $request, array $body): bool
    {
        return $this->isRequestBodyValid($body) && $this->isRequestSecure($request, $body);
    }

    private function isRequestSecure(Request $request, array $body): bool
    {
        // todo implement request security check

        return true;
    }

    private function isRequestBodyValid(array $body): bool
    {
        if (!isset($body['auto_renew_adam_id'])) {
            return false;
        }

        if (!isset($body['auto_renew_product_id'])) {
            return false;
        }

        if (!isset($body['notification_type'])) {
            return false;
        }

        if (!isset($body['auto_renew_status_change_date'])) {
            return false;
        }

        return true;
    }
}
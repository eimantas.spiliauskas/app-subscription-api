<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Validator;

use Symfony\Component\HttpFoundation\Request;

interface NotificationValidatorInterface
{
    public function isValid(Request $request, array $body): bool;
}
<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Validator;

use App\Bundle\SubscriptionBundle\Exception\NotificationValidatorNotFoundException;
use App\Bundle\SubscriptionBundle\Service\Validator\NotificationValidatorInterface;
use Symfony\Component\HttpFoundation\Request;

class SubscriptionNotificationRequestValidator
{
    private array $validators;

    public function __construct()
    {
        $this->validators = [];
    }

    public function addValidator(NotificationValidatorInterface $validator, string $provider)
    {
        $this->validators[$provider] = $validator;
    }

    public function isValid(Request $request, array $data, string $provider): bool
    {
        $validator = $this->getValidator($provider);
        return $validator->isValid($request, $data);
    }

    public function getValidator(string $type):NotificationValidatorInterface
    {
        if (!isset($this->validators[$type])) {
            throw new NotificationValidatorNotFoundException('Decoder not found by type: ' . $type);
        }

        return $this->validators[$type];
    }
}
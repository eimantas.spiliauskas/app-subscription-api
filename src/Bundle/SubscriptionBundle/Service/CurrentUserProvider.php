<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service;

use App\Bundle\SubscriptionBundle\Entity\User;
use App\Bundle\SubscriptionBundle\Repository\UserRepository;

class CurrentUserProvider
{
    public UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getIdentifierUser(): User
    {
        return $this->userRepository->getById(1);
    }
}
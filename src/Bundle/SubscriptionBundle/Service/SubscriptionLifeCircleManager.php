<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service;

use App\Bundle\SubscriptionBundle\Entity\Notification;
use App\Bundle\SubscriptionBundle\Entity\Subscription;
use App\Bundle\SubscriptionBundle\Repository\SubscriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use DateTime;
use Ramsey\Uuid\Uuid;

class SubscriptionLifeCircleManager
{
    private EntityManagerInterface $entityManager;
    private SubscriptionRepository $subscriptionRepository;
    private CurrentUserProvider $currentUserProvider;

    public function __construct(
        EntityManagerInterface $entityManager,
        SubscriptionRepository $subscriptionRepository,
        CurrentUserProvider $currentUserProvider
    ) {
        $this->entityManager = $entityManager;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->currentUserProvider = $currentUserProvider;
    }

    public function handleNotification(Notification $notification): Subscription
    {
        $subscription = null;
        $notificationType = $notification->getType();

        if ($notificationType === Notification::TYPE_CREATE) {
            $subscription =$this->createSubscriptionFromNotification($notification);
        } elseif ($notificationType === Notification::TYPE_UPDATE) {
            $subscription = $this->updateSubscriptionByNotification($notification);
        } elseif ($notificationType === Notification::TYPE_CANCEL) {
            $subscription = $this->cancelSubscriptionByNotification($notification);
        }


        // TODO dispatch event to inform other internal api's

        return $subscription;
    }

    private function createSubscriptionFromNotification(Notification $notification): Subscription
    {
        $subscription = (new Subscription())
            ->setToken(Uuid::uuid4()->toString())
            ->setProvider($notification->getProvider())
            ->setUser($this->currentUserProvider->getIdentifierUser())
            ->setProviderIdentifier($notification->getSubscriptionIdentifier())
            ->setProductId($notification->getProduct())
            ->setStatus(Subscription::STATUS_ACTIVE)
        ;

        $this->entityManager->persist($subscription);

        return $subscription;
    }

    private function updateSubscriptionByNotification(Notification $notification): Subscription
    {
        $subscription = $this->subscriptionRepository->findByIdentifier($notification->getSubscriptionIdentifier());

        if ($subscription === null) {
            throw new InvalidArgumentException('subscription not found');
        }

        $subscription->setUpdatedAt(new DateTime());
        $subscription->setStatus(Subscription::STATUS_ACTIVE);

        return $subscription;
    }

    private function cancelSubscriptionByNotification(Notification $notification): Subscription
    {
        $subscription = $this->subscriptionRepository->findByIdentifier($notification->getSubscriptionIdentifier());

        if ($subscription === null) {
            throw new InvalidArgumentException('subscription not found');
        }

        return $subscription
            ->setStatus(Subscription::STATUS_CANCELED)
            ->setUpdatedAt(new DateTime())
        ;
    }
}

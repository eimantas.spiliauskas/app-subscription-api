<?php

namespace App\Bundle\SubscriptionBundle\Service\Decoder;

use App\Bundle\SubscriptionBundle\Exception\DecodingException;

class Json implements DecoderInterface
{
    public function decode(string $body): array
    {
        $decodedBody = json_decode($body, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new DecodingException('got exception durring decoding', json_last_error());
        }

        return $decodedBody;
    }
}

<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Decoder;

interface DecoderInterface
{
    public function decode(string $body): array;
}
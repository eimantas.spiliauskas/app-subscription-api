<?php

namespace App\Bundle\SubscriptionBundle\Service\Decoder;

use App\Bundle\SubscriptionBundle\Exception\DecoderNotFoundException;

class Decoder
{
    public array $decoders;

    public function __construct()
    {
        $this->decoders = [];
    }

    public function addDecoder(DecoderInterface $decoder, string $type): void
    {
        $this->decoders[$type] = $decoder;
    }

    public function decode(string $body, $type): array
    {
        $decoder = $this->getDecoder($type);
        return $decoder->decode($body);
    }

    private function getDecoder(string $decoderType): DecoderInterface
    {
        if (!isset($this->decoders[$decoderType])) {
            throw new DecoderNotFoundException('Decoder not found by type: ' . $decoderType);
        }

        return $this->decoders[$decoderType];
    }
}
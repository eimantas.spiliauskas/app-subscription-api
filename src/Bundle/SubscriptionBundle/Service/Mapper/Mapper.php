<?php

declare(strict_types=1);

namespace App\Bundle\SubscriptionBundle\Service\Mapper;

use App\Bundle\SubscriptionBundle\Entity\Notification;
use App\Bundle\SubscriptionBundle\Exception\MapperNotFoundException;
use App\Normalizer\NormalizerInterface;

class Mapper
{
    private array $mappers;

    public function __construct()
    {
        $this->mappers = [];
    }

    public function addMapper(NormalizerInterface $mapper, string $provider): void
    {
        $this->mappers[$provider] = $mapper;
    }

    public function map(array $data, string $provider): Notification
    {
        $mapper = $this->getMapper($provider);
        return $mapper
            ->mapToEntity($data)
            ->setProvider($provider)
        ;
    }

    public function getMapper(string $type): NormalizerInterface
    {
        if (!isset($this->mappers[$type])) {
            throw new MapperNotFoundException('Mapper not found by type: ' . $type);
        }

        return $this->mappers[$type];
    }
}

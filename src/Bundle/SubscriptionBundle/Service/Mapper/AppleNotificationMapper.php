<?php

namespace App\Bundle\SubscriptionBundle\Service\Mapper;

use App\Bundle\SubscriptionBundle\Entity\Notification;
use App\Bundle\SubscriptionBundle\StatusResolver;
use App\Normalizer\NormalizerInterface;
use DateTime;

class AppleNotificationMapper implements NormalizerInterface
{
    public function mapToEntity(array $data): Notification
    {
        return (new Notification())
            ->setDate(new DateTime($data['auto_renew_status_change_date']))
            ->setProduct($data['auto_renew_product_id'])
            ->setType(StatusResolver::getNotificationStatus($data['notification_type']))
            ->setSubscriptionIdentifier($data['auto_renew_adam_id'])
        ;
    }
}
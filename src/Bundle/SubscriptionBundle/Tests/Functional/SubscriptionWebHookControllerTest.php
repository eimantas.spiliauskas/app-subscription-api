<?php

namespace App\Bundle\SubscriptionBundle\Tests\Functional;

use App\Bundle\AssetsBundle\Tests\Functional\fixtures\WebHookFixture;
use App\Bundle\SubscriptionBundle\Entity\WebHook;
use App\Tests\DatabaseAwareClientTestCase;
use Symfony\Component\HttpFoundation\Request;

class SubscriptionWebHookControllerTest extends DatabaseAwareClientTestCase
{
    public function setUp(): void
    {
        $this->createClientWithDatabase(__DIR__ . '/fixtures');
    }

    public function testGetOnlyNewWebHooks()
    {
        $payload = [
            'status' => WebHook::STATUS_NEW,
            'limit' => 2,
        ];

        $response = $this->request(
            Request::METHOD_GET,
            '/web-hooks/rest/v1/web-hooks?' . http_build_query($payload)
        );
        $content = json_decode($response->getContent(), true);

        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertCount(1, $content['items']);
        $this->assertEquals(1, $content['total_count']);
        $this->assertEquals(WebHookFixture::HASH_1, $content['items'][0]['hash']);
    }
}
<?php

declare(strict_types=1);

namespace App\Bundle\AssetsBundle\Tests\Functional\fixtures;

use App\Bundle\SubscriptionBundle\Entity\WebHook;
use App\Bundle\SubscriptionBundle\SubscriptionProviders;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class WebHookFixture extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    const HASH_1 = 'aaa';
    const HASH_2 = 'bbb';

    public function load(ObjectManager $manager)
    {
        $webHook1 = (new WebHook())
            ->setStatus(WebHook::STATUS_NEW)
            ->setData([])
            ->setProvider(SubscriptionProviders::PROVIDER_APPLE)
            ->setHash(self::HASH_1)
        ;

        $webHook2 = (new WebHook())
            ->setStatus(WebHook::STATUS_PROCESSED)
            ->setData([])
            ->setProvider(SubscriptionProviders::PROVIDER_APPLE)
            ->setHash(self::HASH_2)
        ;

        $this->setReference('web_hook_1', $webHook1);
        $this->setReference('web_hook_2', $webHook2);

        $manager->persist($webHook1);
        $manager->persist($webHook2);
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
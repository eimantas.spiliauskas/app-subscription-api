<?php

declare(strict_types=1);

namespace App\Bundle\AssetsBundle\Tests\Functional\fixtures;

use App\Bundle\SubscriptionBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = (new User())
            ->setPassword('aaa')
            ->setToken('bbb')
        ;

        $this->setReference('user', $user);

        $manager->persist($user);
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
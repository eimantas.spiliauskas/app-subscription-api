<?php

namespace App\Tests;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DatabaseAwareClientTestCase extends WebTestCase
{
    private EntityManagerInterface $entityManager;
    protected KernelBrowser $client;

    public function createClientWithDatabase(string $fixturesDirectory = null): KernelBrowser
    {
        $this->client = static::createClient();
        $container = $this->client->getContainer();
        $this->entityManager = $container->get('doctrine.orm.entity_manager');

        $meta = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($this->entityManager);

        $schemaTool->dropDatabase();
        $schemaTool->createSchema($meta);

        if ($fixturesDirectory !== null) {
            $loader = new Loader();
            $loader->loadFromDirectory($fixturesDirectory);

            $purger = new ORMPurger();
            $executor = new ORMExecutor($this->entityManager, $purger);
            $executor->execute($loader->getFixtures());
        }

        return $this->client;
    }

    public function request(string $method, string $url, array $data = []): Response
    {
        $this->client->request(
            $method,
            $url,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        return $this->client->getResponse();
    }

    public function tearDown(): void
    {
        (new SchemaTool($this->entityManager))->dropDatabase();
    }
}
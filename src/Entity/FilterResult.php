<?php

declare(strict_types=1);

namespace App\Entity;

class FilterResult
{
    private array $items;
    private int $totalCount;

    public function __construct()
    {
        $this->items = [];
        $this->totalCount = 0;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $totalCount): self
    {
        $this->totalCount = $totalCount;
        return $this;
    }
}

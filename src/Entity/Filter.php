<?php

declare(strict_types=1);

namespace App\Entity;

class Filter
{
    private int $limit;

    public function __construct()
    {
        $this->limit = 100;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }
}

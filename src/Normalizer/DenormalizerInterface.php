<?php

declare(strict_types=1);

namespace App\Normalizer;

interface DenormalizerInterface
{
    public function mapFromEntity(object $data): array ;
}

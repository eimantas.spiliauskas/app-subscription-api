<?php

declare(strict_types=1);

namespace App\Normalizer;

use App\Entity\FilterResult;

class FilterResultNormalizer implements DenormalizerInterface
{
    private DenormalizerInterface $itemDenormalizer;

    public function __construct(DenormalizerInterface $itemDenormalizer)
    {
        $this->itemDenormalizer = $itemDenormalizer;
    }

    /**
     * @param FilterResult $entity
     * @return array
     */
    public function mapFromEntity(object $entity): array
    {
        $items = array_map(
            function (object $item) {
                return $this->itemDenormalizer->mapFromEntity($item);
            },
            $entity->getItems()
        );

        return [
            'items' => $items,
            'total_count' => $entity->getTotalCount(),
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Normalizer;

interface NormalizerInterface
{
    public function mapToEntity(array $data): object;
}

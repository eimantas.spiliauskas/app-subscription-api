<?php

declare(strict_types=1);

namespace App\Normalizer;

use App\Entity\Filter;

class FilterNormalizer
{
    public function mapBase(Filter $object, array $data): void
    {
        if (!empty($data['limit'])) {
            $object->setLimit((int) $data['limit']);
        }
    }
}
